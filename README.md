# Flutter_BP

This project is configured with build flavors. This guide provides instructions for running and building the project.

## Running in Debug Mode
To run the project in debug mode, follow these steps:

1. Open Visual Studio Code (VSCode).
2. Go to the "Run and Debug" section in VSCode.
3. In the top section, locate the play button next to the environment dropdown. Click on the dropdown and select your desired environment.
4. Click the play button to run the project based on your preferences.

## Building APKs with Flavors
To build APKs based on different flavors, you can use the following commands:


### Run app via terminal
```shell
flutter run --flavor dev --target lib/main_dev.dart
flutter run --flavor prod --target lib/main_prod.dart
```



### Dev APK
To build a dev APK, use the following command:
```shell
flutter build apk --flavor dev --target lib/main_dev.dart
```
### Live APK
To build a dev APK, use the following command:
```shell
flutter build apk --flavor prod --target lib/main_prod.dart
```
