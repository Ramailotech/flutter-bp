class NetworkConstants {
  static const String baseUrl = "www.ramailotech.com/";
  static const String loginEndpoint = "${baseUrl}login/";
  static const String registerEndpoint = "${baseUrl}register/";
  static const Map<String, String> headerWithoutToken = {
    'Content-Type': 'application/json'
  };
}
