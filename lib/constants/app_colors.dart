import 'package:flutter/material.dart';

class AppColors {
  static const Color primaryColor = Color(0xFFfe623c);
  static const Color successColor = Color(0xFF1DD75B);
  static const Color lightTextColor = Color(0xFF344563);

  static const Color darkScaffoldColor = Color(0xFF111827);
  static const Color darkTextColor = Color(0xFFFFFFFF);
}
