///fyi: keeping all the keys of shared preferences here

class SharedPrefsKeys {
  static const String accessToken = "access_token";
  static const String refreshToken = "refresh_token";
  static const String isDarkMode = "is_dark_mode";
  static const String selectedLocale = "selected_locale";

  /// add more keys below
}
