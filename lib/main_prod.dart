import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bp/firebase/firebase_setup.dart';
import 'package:flutter_bp/my_app.dart';
import 'package:flutter_bp/utils/service_locator.dart';

void main() async {
  WidgetsFlutterBinding();
  FirebaseSetup();
  await setUpLocator();
  runApp(const MyApp(
    title: "Prod app",
  ));
}
