import 'dart:convert';
import 'dart:io';
import 'package:dio/dio.dart';
import 'package:flutter_bp/core/network/client/base_client.dart';
import 'package:flutter_bp/core/network/functions/get_header.dart';
import 'package:flutter_bp/core/utils/logger/debug_log.dart';
import 'package:flutter_bp/utils/dialog_utils.dart';
import 'package:flutter_bp/utils/service_locator.dart';
import 'package:shared_preferences/shared_preferences.dart';

class BaseClientImpl extends BaseClient {
  @override
  Future<Response<dynamic>?> getRequest({
    String baseUrl = "",
    Map<String, String>? optionalHeaders,
    Map<String, dynamic>? queryParameters,
    required String path,
    bool showDialog = false,
    bool shouldCache = true,
  }) async {
    Response? response;
    if (showDialog) showLoadingDialog();

    try {
      Map<String, String> header = getHeader();
      if (optionalHeaders != null) {
        header.addAll(optionalHeaders);
      }
      response = await Dio().get(
        baseUrl + path,
        queryParameters: queryParameters,
        options: Options(
          headers: header,
          sendTimeout: const Duration(seconds: 40),
          receiveTimeout: const Duration(seconds: 40),
        ),
      );
      debugLog(baseUrl + path + response.statusCode.toString());
      if (shouldCache && response.data['status-code'] == 1) {
        locator<SharedPreferences>().setString(
          path,
          jsonEncode(response.data).toString(),
        );
      }
    } on DioException catch (e) {
      debugLog(e.toString());
      if (shouldCache && e.error is SocketException) {
        response = getCachedResponse(path);
      }
    } catch (e) {
      debugLog(e.toString());
      if (shouldCache && e is SocketException) {
        response = getCachedResponse(path);
      }
    }
    if (showDialog) hideLoadingDialog();
    return response;
  }

  Response? getCachedResponse(String endpoint) {
    String? cachedResponse = locator<SharedPreferences>().getString(endpoint);
    Response? response = cachedResponse != null
        ? Response(
            requestOptions: RequestOptions(),
            data: cachedResponse,
            statusCode: 200,
          )
        : null;
    return response;
  }

  @override
  Future<Response?> postRequest({
    String baseUrl = "",
    Map<String, String>? optionalHeaders,
    Map<String, dynamic>? data,
    required String path,
    bool showDialog = false,
  }) async {
    Response? response;
    if (showDialog) {
      showLoadingDialog();
    }
    try {
      Map<String, String> header = getHeader();
      if (optionalHeaders != null) {
        header.addAll(optionalHeaders);
      }
      response = await Dio().post(
        baseUrl + path,
        options: Options(
          headers: header,
          sendTimeout: const Duration(seconds: 40),
          receiveTimeout: const Duration(seconds: 40),
        ),
        data: data,
      );
    } catch (e) {
      debugLog(e.toString());
    }
    if (showDialog) hideLoadingDialog();
    return response;
  }
}
