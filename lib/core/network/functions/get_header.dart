import 'package:flutter_bp/constants/shared_prefs_constants.dart';
import 'package:flutter_bp/utils/service_locator.dart';
import 'package:shared_preferences/shared_preferences.dart';

Map<String, String> getHeader({
  bool requiresAuthorization = true,
}) {
  String? token =
      locator<SharedPreferences>().getString(SharedPrefsKeys.accessToken);
  return {
    "Content-Type": "application/json",
    if (token != null && requiresAuthorization)
      "Authorization": "Bearer $token",
  };
}
