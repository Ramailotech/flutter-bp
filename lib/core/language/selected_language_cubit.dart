import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_bp/constants/shared_prefs_constants.dart';
import 'package:flutter_bp/utils/service_locator.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SelectedLanguageCubit extends Cubit<Locale> {
  SelectedLanguageCubit() : super(const Locale("en"));

  updateLocale(String locale) {
    emit(Locale(locale));
    locator<SharedPreferences>().setString(
      SharedPrefsKeys.selectedLocale,
      locale,
    );
  }

  getLocale() {
    String currentLocale = locator<SharedPreferences>()
            .getString(SharedPrefsKeys.selectedLocale) ??
        "en";
    emit(Locale(currentLocale));
  }
}
