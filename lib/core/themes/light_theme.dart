import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bp/constants/app_colors.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

final lightTheme = ThemeData(
  useMaterial3: true,
  // fontFamily: AppConstants.appFont,
  primaryColor: AppColors.primaryColor,
  colorScheme: const ColorScheme(
    brightness: Brightness.light,
    primary: AppColors.primaryColor,
    onPrimary: AppColors.primaryColor,
    secondary: Colors.white,
    onSecondary: Colors.white,
    error: Colors.red,

    onError: Colors.red,
    // error: AppColors.errorColor,
    // onError: AppColors.errorColor,
    background: Colors.white,
    onBackground: Colors.white,
    // onBackground: AppColors.,
    surface: Colors.white,
    onSurface: AppColors.primaryColor,
  ),
  // indicatorColor: AppColors.lightTextColor,
  checkboxTheme: CheckboxThemeData(
    fillColor: MaterialStateProperty.all(AppColors.primaryColor),
    checkColor: MaterialStateProperty.all(Colors.white),
  ),
  // tabBarTheme: TabBarTheme(
  //   // unselectedLabelColor: AppColors.lightBodyColor,
  //   unselectedLabelStyle:
  //       TextStyle(fontSize: 15.sp, fontWeight: FontWeight.w500),
  //   labelStyle: TextStyle(fontSize: 15.sp, fontWeight: FontWeight.w500),
  //   labelColor: AppColors.primaryColor,
  // ),
  // dividerColor: Colors.grey,
  dividerTheme: DividerThemeData(
    color: Colors.grey.withOpacity(0.5),
  ),
  dialogBackgroundColor: Colors.white,

  dialogTheme: DialogTheme(
    backgroundColor: Colors.white,
    contentTextStyle: TextStyle(
      color: AppColors.lightTextColor,
      fontSize: 16.sp,
      fontWeight: FontWeight.normal,
    ),
    titleTextStyle: TextStyle(
      color: AppColors.lightTextColor,
      fontSize: 18.sp,
      fontWeight: FontWeight.normal,
    ),
  ),

  radioTheme: RadioThemeData(
      fillColor: MaterialStateProperty.all(AppColors.primaryColor)),
  cardColor: Colors.white,
  switchTheme: SwitchThemeData(
      thumbColor: MaterialStateProperty.all<Color>(AppColors.primaryColor),
      trackColor:
          MaterialStateProperty.all(const Color.fromRGBO(35, 116, 225, 0.2))),
  splashColor: AppColors.primaryColor,
  scaffoldBackgroundColor: Colors.white,
  elevatedButtonTheme: ElevatedButtonThemeData(
    style: ButtonStyle(
      backgroundColor: MaterialStateProperty.all(
        AppColors.primaryColor,
      ),
      textStyle: MaterialStateProperty.all(
        TextStyle(
          color: Colors.white,
          // fontFamily: AppConstants.ibmFont,
          fontSize: 18.sp,
        ),
      ),
      overlayColor: MaterialStateProperty.all(Colors.white),
    ),
  ),
  appBarTheme: AppBarTheme(
    // backgroundColor: AppColors.primaryColor,
    backgroundColor: Colors.white,
    systemOverlayStyle: Platform.isIOS
        ? SystemUiOverlayStyle.dark
        : const SystemUiOverlayStyle(
            statusBarColor: Colors.transparent,
            statusBarIconBrightness: Brightness.dark,
          ),
    titleTextStyle: TextStyle(
      color: Colors.black,

      // fontFamily: AppConstants.ibmFont,
      fontSize: 20.sp,
    ),

    actionsIconTheme: const IconThemeData(color: AppColors.lightTextColor),
    // iconTheme: Ico

    // actionsIconTheme: const IconThemeData(
    // color: AppColors.lightBodyColor,
    // ),
  ),
  // bottomAppBarTheme: BottomAppBarTheme(
  //     // color: AppColors.lightScaffoldColor,
  //     ),
  // bottomAppBarColor: AppColors.lightScaffoldColor,
  hintColor: Colors.grey,
  iconTheme: const IconThemeData(
    color: Colors.grey,
  ),
  textTheme: TextTheme(
    titleLarge: TextStyle(
      color: AppColors.lightTextColor,
      fontSize: 18.sp,
      fontWeight: FontWeight.w600,
      // fontFamily: AppConstants.ibmFont,
    ),
    headlineLarge: TextStyle(
      color: AppColors.lightTextColor,
      fontSize: 24.sp,
      fontWeight: FontWeight.bold,
    ),
    titleSmall: TextStyle(
      color: AppColors.lightTextColor,
      fontSize: 14.sp,
      // fontFamily: AppConstants.ibmFont,
      fontWeight: FontWeight.w400,
    ),
    titleMedium: TextStyle(
      color: AppColors.lightTextColor,
      fontSize: 16.sp,
      fontWeight: FontWeight.w400,
    ),
    displayLarge: TextStyle(
      color: AppColors.lightTextColor,
      fontSize: 28.sp,
      // fontFamily: AppConstants.ibmFont,
      fontWeight: FontWeight.w600,
    ),
    displayMedium: TextStyle(
      color: AppColors.lightTextColor,
      fontSize: 24.sp,
      // fontFamily: AppConstants.ibmFont,
      fontWeight: FontWeight.w600,
    ),
    displaySmall: TextStyle(
      color: AppColors.lightTextColor,
      fontSize: 21.sp,
      // fontFamily: AppConstants.ibmFont,
      fontWeight: FontWeight.w600,
    ),
    bodyLarge: TextStyle(
      fontSize: 14.5.sp,
      fontWeight: FontWeight.w400,

      color: AppColors.lightTextColor,
      // fontFamily: AppConstants.ibmFont,
    ),
    bodyMedium: TextStyle(
      color: AppColors.lightTextColor,
      fontSize: 13.sp,
      // fontFamily: AppConstants.ibmFont,
      fontWeight: FontWeight.w400,
    ),
    bodySmall: TextStyle(
      color: AppColors.lightTextColor,
      // fontFamily: AppConstants.ibmFont,
      fontWeight: FontWeight.w400,
      fontSize: 12.sp,
    ),
  ),
  floatingActionButtonTheme: const FloatingActionButtonThemeData(
    foregroundColor: Colors.white,
    backgroundColor: AppColors.primaryColor,
  ),
);
