import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bp/constants/app_colors.dart';
import 'package:flutter_bp/constants/shared_prefs_constants.dart';
import 'package:flutter_bp/core/themes/dark_theme.dart';
import 'package:flutter_bp/core/themes/light_theme.dart';
import 'package:flutter_bp/utils/service_locator.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ThemeCubit extends Cubit<ThemeData> {
  ThemeCubit() : super(lightTheme);

  getTheme() async {
    bool? isDark =
        locator<SharedPreferences>().getBool(SharedPrefsKeys.isDarkMode);
    if (isDark != null && isDark) {
      emit(darkTheme);
      changeOverlayColor(true);
    } else {
      emit(lightTheme);
      changeOverlayColor(false);
    }
  }

  changeTheme(ThemeData currentTheme) async {
    bool isDarkCurrently = currentTheme == darkTheme;
    locator<SharedPreferences>()
        .setBool(SharedPrefsKeys.isDarkMode, !isDarkCurrently);
    changeOverlayColor(!isDarkCurrently);
    emit(isDarkCurrently ? lightTheme : darkTheme);
  }
}

changeOverlayColor(bool isDark) {
  SystemChrome.setSystemUIOverlayStyle(
    SystemUiOverlayStyle(
      statusBarColor:
          isDark ? AppColors.darkScaffoldColor : AppColors.primaryColor,
    ),
  );
}
