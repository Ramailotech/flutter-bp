import 'dart:ui';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/cupertino.dart';

class FirebaseSetup {
  //Constructor for Firebase Setup
  FirebaseSetup() {
    initializeFirebase();
  }

  //Initilalizing firebase
  Future<void> initializeFirebase() async {
    await Firebase.initializeApp(
      // options: DefaultFirebaseOptions.currentPlatform,
    );

    FlutterError.onError = (errorDetails) {
      FirebaseCrashlytics.instance.recordFlutterFatalError(errorDetails);
    };
    PlatformDispatcher.instance.onError = (error, stack) {
      FirebaseCrashlytics.instance.recordError(error, stack, fatal: true);
      return true;
    };
    initCrashylitcs();
    initializeFirebaseAnalytics();
  }

  void initializeFirebaseAnalytics() {}

  void initCrashylitcs() {}
}
