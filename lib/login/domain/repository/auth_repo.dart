import 'package:dartz/dartz.dart';
import 'package:flutter_bp/core/network/failure/failure.dart';
import 'package:flutter_bp/core/network/success/success.dart';
import 'package:flutter_bp/login/data/models/user_model.dart';

abstract class AuthRepo {
  Future<Either<Success, Failure>> requestForLogin(UserModel usermodel);
}
