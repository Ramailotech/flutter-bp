part of 'auth_bloc.dart';

abstract class AuthState extends Equatable {
  @override
  List<Object?> get props => [];
}

class LoadingState extends AuthState {}

class LoggedOutState extends AuthState {}

class LoggedInState extends AuthState {}

class AuthErrorState extends AuthState {
  final String errorMsg;
  AuthErrorState({required this.errorMsg});
  @override
  List<Object?> get props => [errorMsg];
}

class VerifyOTPState extends AuthState {
  final UserModel userModel;

  VerifyOTPState({required this.userModel});
  @override
  List<Object?> get props => [userModel];
}
