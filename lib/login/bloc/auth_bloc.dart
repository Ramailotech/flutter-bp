import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bp/login/data/models/user_model.dart';
import 'package:flutter_bp/login/domain/repository/auth_repo.dart';
import 'package:flutter_bp/utils/shared_preferences_helper.dart';

part 'auth_event.dart';
part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final AuthRepo _repo;
  AuthBloc(this._repo) : super(LoadingState()) {
    on<AuthCheckerEvent>(_authChecker);
    on<LoginClickedEvent>(_loginClick);
    on<LogoutClickedEvent>(_logoutClick);
    // on<RegisterClickedEvent>(_registerClick);
    // on<VerifyOTPClickedEvent>(_otpVerifiedEvent);
  }
  final SharedPrefsHelper sharedPrefsHelper = SharedPrefsHelper();

  ///fyi: for checking if user has a loggedin session or not
  /// (useful when app is opened, should navigate to home or login screen based on auth status)
  FutureOr<void> _authChecker(
    AuthCheckerEvent authCheckerEvent,
    Emitter<AuthState> emit,
  ) async {
    emit.call(LoadingState());
    final String? accessToken = await sharedPrefsHelper.getAccessToken();
    final String? refreshToken = await sharedPrefsHelper.getRefreshToken();
    if (accessToken == null || refreshToken == null) {
      emit.call(LoggedOutState());
    } else {
      emit.call(LoggedInState());
    }
  }

  ///fyi: when user clicks login button
  FutureOr<void> _loginClick(
    LoginClickedEvent event,
    Emitter<AuthState> emit,
  ) async {
    emit(LoadingState());
    final response = await _repo.requestForLogin(event.userModel);
    emit(
      response.fold(
        (l) => LoggedInState(),
        (r) => AuthErrorState(
          errorMsg: r.message!,
        ),
      ),
    );
  }

  ///fyi: when user clicks logout button
  FutureOr<void> _logoutClick(
    LogoutClickedEvent event,
    Emitter<AuthState> emit,
  ) async {
    // clear records of shared pereferences
    sharedPrefsHelper.clearAll();
    emit.call(LoggedOutState());
  }
}
