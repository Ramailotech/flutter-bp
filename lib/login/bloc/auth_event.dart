part of 'auth_bloc.dart';

abstract class AuthEvent extends Equatable {
  const AuthEvent();

  @override
  List<Object> get props => [];
}

class AuthCheckerEvent extends AuthEvent {}

class LogoutClickedEvent extends AuthEvent {}

class LoginClickedEvent extends AuthEvent {
  final UserModel userModel;
  const LoginClickedEvent({required this.userModel});

  @override
  List<Object> get props => [userModel];
}

class RegisterClickedEvent extends AuthEvent {
  final UserModel personTryingToRegister;

  const RegisterClickedEvent({required this.personTryingToRegister});

  @override
  List<Object> get props => [personTryingToRegister];
}

class VerifyOTPClickedEvent extends AuthEvent {
  final String emailOrUserame;
  final String otp;

  const VerifyOTPClickedEvent(this.emailOrUserame, this.otp);
  @override
  List<Object> get props => [emailOrUserame, otp];
}
