import 'package:dartz/dartz.dart';
import 'package:flutter_bp/core/network/client/base_client.dart';
import 'package:flutter_bp/core/network/failure/failure.dart';
import 'package:flutter_bp/core/network/functions/get_parsed_data.dart';
import 'package:flutter_bp/core/network/success/success.dart';
import 'package:flutter_bp/login/data/models/user_model.dart';
import 'package:flutter_bp/login/domain/repository/auth_repo.dart';

class AuthImpl implements AuthRepo {
  final BaseClient _client;
  AuthImpl(this._client);

  //Instead of success, we will be returning custom model from here,
  //the model will be as per the response. and inside getParsedData function
  //send Model.fromJson as well
  @override
  Future<Either<Success, Failure>> requestForLogin(UserModel usermodel) async {
    final response = await _client.getRequest(path: "/login");
    return getParsedData(
      response,
      Success.fromJson,
    );
  }
}
