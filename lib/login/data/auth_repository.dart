// import 'dart:convert';

// import 'package:flutter_bp/login/data/auth_provider.dart';
// import 'package:flutter_bp/login/data/models/user_model.dart';
// import 'package:flutter_bp/login/data/models/user_response.dart';
// import 'package:http/http.dart' as http;

// class AuthRepository {
//   static Future<UserResponse> requestLogin(UserModel userModel) async {
//     http.Response rawResponseOfLogin =
//         await AuthProvider.loginPostReq(userModel: userModel);

//     /// fyi: backend might be returning data in diff format in body,,
//     /// so need to customize below json decoding according to the need
//     return UserResponse.fromJson(jsonDecode(rawResponseOfLogin.body));
//   }
// }
