// import 'package:flutter_bp/constants/network_constants.dart';
// import 'package:flutter_bp/login/data/models/user_model.dart';
// import 'package:http/http.dart' as http;

// ///fyi: backend might have different formats of receiving body parameters,
// ///so need to customize below methods according to the project
// class AuthProvider {
//   // for login req
//   static Future<http.Response> loginPostReq(
//       {required final UserModel userModel}) async {
//     return http.post(Uri.parse(NetworkConstants.loginEndpoint),
//         headers: NetworkConstants.headerWithoutToken, body: userModel.toJson());
//   }

//   // for user register req
//   static Future<http.Response> reqisterPostReq(
//       {required final UserModel userModel}) {
//     return http.post(Uri.parse(NetworkConstants.registerEndpoint),
//         headers: NetworkConstants.headerWithoutToken,
//         body: userModel.toString());
//   }
// }
