import 'package:flutter_bp/login/data/models/user_model.dart';

class UserResponse {
  final bool? success;
  final String? msg;
  final UserModel? data;

  UserResponse({this.success, this.data, this.msg});
  static fromJson(Map<String, dynamic> json) {
    return UserResponse(
        success: json['success'],
        msg: json['msg'],
        data: UserModel.fromJson(json['data']));
  }
}
