///fyi: sample of user model, can be customized

class UserModel {
  final String? uid;
  final String? email;
  final String? name;
  final String? password;

  UserModel({this.uid, this.email, this.name, this.password});
  static fromJson(Map<String, dynamic> json) {
    return UserModel(
        uid: json['uid'],
        email: json['email'],
        name: json['name'],
        password: json['password']);
  }

  Map<String, dynamic> toJson() =>
      {"uid": uid, "email": email, "name": name, "password": password};
}
