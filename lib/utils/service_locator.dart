import 'package:flutter_bp/core/network/client/base_client.dart';
import 'package:flutter_bp/core/network/client/base_client_implementation.dart';
import 'package:flutter_bp/login/data/implementation/auth_implementation.dart';
import 'package:flutter_bp/login/domain/repository/auth_repo.dart';
import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';

final locator = GetIt.instance;

setUpLocator() async {
  SharedPreferences sharedPreferencesInstance =
      await SharedPreferences.getInstance();

  locator.registerSingleton<SharedPreferences>(sharedPreferencesInstance);
  locator.registerFactory<BaseClient>(() => BaseClientImpl());

  locator.registerFactory<AuthRepo>(() => AuthImpl(locator()));
}
